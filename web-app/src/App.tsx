import React from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import store from './state/store';
import {fetchPayments} from './state/payments';
import Home from './pages/Home/Home';
import Add from './pages/Add/Add';
import Payment from './pages/Payment/Payment';

store.dispatch(fetchPayments());

const App = () => (
    <Provider store={store}>
        <Router>
            <Switch>
                <Route exact path="/">
                    <Home />
                </Route>
                <Route exact path="/add">
                    <Add />
                </Route>
                <Route exact path="/payment/:id">
                    <Payment />
                </Route>
            </Switch>
        </Router>
    </Provider>
);

export default App;
