import React from 'react';
import {Form, FormikProps} from 'formik';
import InputField from './InputField';
import Select from './Select';
import {PaymentFrequency} from '../api/paymentApi';
import './paymentForm.css';

export interface PaymentFormValues {
    name: string;
    amount: number;
    startDate: string;
    frequency: PaymentFrequency;
}

interface PaymentFrequencyOption {
    value: PaymentFrequency;
    text: string;
}

const paymentFrequencyOptions: PaymentFrequencyOption[] = [
    {value: PaymentFrequency.WEEKLY, text: 'Weekly'},
    {value: PaymentFrequency.MONTHLY, text: 'Monthly'},
    {value: PaymentFrequency.ANNUALLY, text: 'Annually'},
];

interface PaymentFormProps extends FormikProps<PaymentFormValues> {
    children: React.ReactNode;
}

export const PaymentForm = (props: PaymentFormProps) => {
    const { values, handleChange,handleBlur, children } = props;

    return (
        <Form className="paymentForm">
            <InputField
                label="Name"
                name="name"
                type="text"
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <InputField
                label="Amount"
                name="amount"
                type="number"
                value={values.amount}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <InputField
                label="Start date"
                name="startDate"
                type="date"
                value={values.startDate}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <Select
                label="Frequency"
                name="frequency"
                value={values.frequency}
                options={paymentFrequencyOptions}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            {children}
        </Form>
    );
};

export default PaymentForm;