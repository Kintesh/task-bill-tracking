import React from 'react';
import {render} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Select from '../Select';

describe('Select', () => {
    it('should render select with label', async () => {
        const onBlurMock = jest.fn();
        const onChangeMock = jest.fn();
        const { getByLabelText } = render(
            <Select
                label="Pick one"
                name="picker"
                options={[{value: '1', text: 'One'}, {value: '2', text: 'Two'}, {value: '3', text: 'Three'}]}
                value="1"
                onBlur={onBlurMock}
                onChange={onChangeMock}
            />
        );

        expect(getByLabelText('Pick one')).toBeInTheDocument();
        userEvent.selectOptions(getByLabelText('Pick one'), '3');
        expect(onChangeMock).toHaveBeenCalled();
    });
});