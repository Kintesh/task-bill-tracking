import React from 'react';
import {render} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import InputField from '../InputField';

describe('InputField', () => {
    it('should render input with label', async () => {
        const onBlurMock = jest.fn();
        const onChangeMock = jest.fn();
        const { getByLabelText } = render(
            <InputField
                label="Amount"
                name="amount"
                type="number"
                value="100"
                onBlur={onBlurMock}
                onChange={onChangeMock}
            />
        );

        expect(getByLabelText('Amount')).toBeInTheDocument();
        await userEvent.type(getByLabelText('Amount'), '1000');
        expect(onChangeMock).toHaveBeenCalled();
        expect((getByLabelText('Amount') as HTMLInputElement).value).toEqual('100');
    });
});