import React from 'react';
import {Formik, FormikProps} from 'formik';
import {render, wait} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import PaymentForm, {PaymentFormValues} from '../PaymentForm';
import {PaymentFrequency} from '../../api/paymentApi';

describe('PaymentForm', () => {
    it('should payment form', async () => {
        const values = {
            name: '',
            amount: 0,
            startDate: '',
            frequency: PaymentFrequency.WEEKLY
        };
        const onSubmitMock = jest.fn();
        const { getByLabelText, getByRole } = render(
            <Formik initialValues={values} onSubmit={onSubmitMock}>
                {(props: FormikProps<PaymentFormValues>) => (
                    <PaymentForm {...props}>
                        <button>Submit</button>
                    </PaymentForm>
                )}
            </Formik>
        );

        await userEvent.type(getByLabelText('Name'), 'Rent');
        await userEvent.type(getByLabelText('Amount'), '100');
        await userEvent.type(getByLabelText('Start date'), '2021-02-19');
        userEvent.selectOptions(getByLabelText('Frequency'), PaymentFrequency.MONTHLY);

        userEvent.click(getByRole('button'));

        await wait(() => {
            expect(onSubmitMock).toHaveBeenCalledWith({
                name: 'Rent',
                amount: 100,
                startDate: '2021-02-19',
                frequency: PaymentFrequency.MONTHLY
            }, expect.anything());
        });
    });
});
