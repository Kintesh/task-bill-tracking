import React from 'react';
import {FormElementProps} from '../types';
import './select.css';

interface SelectProps extends FormElementProps<HTMLSelectElement> {
    options: { value: string; text: string }[];
}

const Select = (props: SelectProps) => {
    const {label, name, value, options, onChange, onBlur} = props;
    return (
        <div className="select">
            <label htmlFor={name}>{label}</label>
            <select id={name} name={name} value={value} onChange={onChange} onBlur={onBlur}>
                {
                    options.map((option) =>
                        <option key={option.value} value={option.value}>{option.text}</option>
                    )
                }
            </select>
        </div>
    );
};

export default Select;