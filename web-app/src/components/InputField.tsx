import React from 'react';
import {FormElementProps} from '../types';
import './inputField.css';

interface InputFieldProps extends FormElementProps<HTMLInputElement> {
    type: string;
}

const InputField = (props: InputFieldProps) => {
    const {label, name, type, value, onChange, onBlur} = props;
    return (
        <div className="inputField">
            <label htmlFor={name}>{label}</label>
            <input
                id={name}
                type={type}
                name={name}
                onChange={onChange}
                onBlur={onBlur}
                value={value}
            />
        </div>
    );
};

export default InputField;