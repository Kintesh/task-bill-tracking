import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {getPayments, Payment} from '../api/paymentApi';

export const fetchPayments = createAsyncThunk('payments/fetchPayments', async () => {
    return await getPayments();
});

const initialState: Payment[] = [];

const paymentsSlice = createSlice({
    name: 'payments',
    initialState,
    reducers: {
        createPayment: (state, action: PayloadAction<Payment>) => {
            console.log('createPayment', action.payload);
            state.push(action.payload);
        },
        updatePayment: (state, action) => {
            console.log('updatePayment', action.payload);
            const payment = state.find(payment => payment.id === action.payload.id);

            if (payment) {
                const index = state.indexOf(payment);
                state[index] = {
                    ...action.payload,
                };
            }
        },
        deletePayment: (state, action) => {
            console.log('deletePayment', action.payload);
            return state.filter(payment => payment.id !== action.payload.id);
        },
    },
    extraReducers: {
        [fetchPayments.fulfilled.type]: (state, action) => {
            return action.payload;
        }
    }
});

const { actions, reducer } = paymentsSlice;
export const { createPayment, updatePayment, deletePayment } = actions;
export default reducer;
