import paymentsReducer, {createPayment, updatePayment, deletePayment} from '../payments';
import {Payment, PaymentFrequency} from '../../api/paymentApi';

describe('payments', () => {
    describe('reducer', () => {
        it('should create a payment', () => {
            const state: Payment[] = [];

            const result = paymentsReducer(state, createPayment({
                id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                name: 'Rent',
                amount: 100,
                startDate: '2021-02-01',
                frequency: PaymentFrequency.MONTHLY,
            }));

            expect(result).toEqual([{
                id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                name: 'Rent',
                amount: 100,
                startDate: '2021-02-01',
                frequency: PaymentFrequency.MONTHLY,
            }]);
        });

        it('should update a payment', () => {
            const state: Payment[] = [{
                id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                name: 'Rent',
                amount: 100,
                startDate: '2021-02-01',
                frequency: PaymentFrequency.MONTHLY,
            }];

            const result = paymentsReducer(state, updatePayment({
                id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                name: 'Rent',
                amount: 1000,
                startDate: '2021-02-01',
                frequency: PaymentFrequency.MONTHLY,
            }));

            expect(result).toEqual([{
                id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                name: 'Rent',
                amount: 1000,
                startDate: '2021-02-01',
                frequency: PaymentFrequency.MONTHLY,
            }]);
        });

        it('should delete a payment', () => {
            const state: Payment[] = [{
                id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                name: 'Rent',
                amount: 100,
                startDate: '2021-02-01',
                frequency: PaymentFrequency.MONTHLY,
            }];

            const result = paymentsReducer(state, deletePayment({
                id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                name: 'Rent',
                amount: 100,
                startDate: '2021-02-01',
                frequency: PaymentFrequency.MONTHLY,
            }));

            expect(result).toEqual([]);
        });
    });

    // TODO: Need to figure this out...
    // describe('extraReducers', () => {
    //     it('should', () => {});
    // });
});
