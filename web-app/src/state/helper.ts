import {TypedUseSelectorHook, useSelector} from 'react-redux';
import {ThunkAction} from 'redux-thunk';
import {Action} from '@reduxjs/toolkit';
import {RootState} from './store';

export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;

export type AppThunk = ThunkAction<void, RootState, null, Action<string>>;
