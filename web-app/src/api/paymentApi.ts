export enum PaymentFrequency {
    WEEKLY = 'weekly',
    MONTHLY = 'monthly',
    ANNUALLY = 'annually',
}

export interface Payment {
    id: string;
    name: string;
    amount: number;
    startDate: string;
    frequency: PaymentFrequency;
}

export const getPayments = async (): Promise<Payment[]> => {
    const request = await fetch('/payments');
    return await request.json() as Payment[];
};

export const postPayment = async (payment: Payment): Promise<Payment> => {
    const request = await fetch('/payments', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payment)
    });
    return await request.json();
};

export const patchPayment = async (id: string, payment: Payment): Promise<Payment> => {
    const request = await fetch(`/payments/${id}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payment)
    });
    return await request.json();
};

export const removePayment = async (id: string): Promise<boolean> => {
    await fetch(`/payments/${id}`, {
        method: 'DELETE',
    });
    return true;
};
