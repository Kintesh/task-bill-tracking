import React from 'react';
import {useHistory} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import {Formik, FormikProps} from 'formik';
import Layout from '../../Layout/Layout';
import {Payment, PaymentFrequency, postPayment} from '../../api/paymentApi';
import {createPayment} from '../../state/payments';
import PaymentForm, {PaymentFormValues} from '../../components/PaymentForm';
import './add.css';

const Add = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const initialValues: PaymentFormValues = {
        name: '',
        amount: 0,
        startDate: '',
        frequency: PaymentFrequency.WEEKLY,
    };

    return (
        <Layout heading="Add a bill">
            <h1 className="center">Enter your details</h1>
            <p className="center">Keep track of your household spending by <br/>adding your bills</p>
            <Formik initialValues={initialValues}
                onSubmit={(values, formikHelpers) => {
                    console.log('Add onSubmit', values);
                    postPayment(values as Payment).then((payment) => {
                        console.log('postPayment - then', payment);
                        dispatch(createPayment(payment));
                        formikHelpers.setSubmitting(false);
                        history.push('/');
                    });
                }}
            >
                {(props: FormikProps<PaymentFormValues>) => (
                    <PaymentForm {...props}>
                        <div className="addButtonContainer">
                            <button className="addButton" type="submit">Add new payment</button>
                        </div>
                    </PaymentForm>
                )}
            </Formik>
        </Layout>
    );
};

export default Add;
