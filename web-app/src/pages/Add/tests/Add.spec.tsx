import React from 'react';
import {configureStore} from '@reduxjs/toolkit';
import * as ReactRedux from 'react-redux';
import {MemoryRouter, Route, Switch} from 'react-router-dom';
import {render, wait} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Add from '../Add';
import * as PaymentApi from '../../../api/paymentApi';
import paymentsReducer from '../../../state/payments';

describe('Add', () => {

    const AddComponent = () => {
        const storeMock = configureStore({reducer: {payments: paymentsReducer}});
        return (
            <ReactRedux.Provider store={storeMock}>
                <MemoryRouter initialEntries={['/add']}>
                    <Switch>
                        <Route exact path="/">
                            <p>Home</p>
                        </Route>
                        <Route path="/add">
                            <Add />
                        </Route>
                    </Switch>
                </MemoryRouter>
            </ReactRedux.Provider>
        );
    };

    it('should render add payment form', async () => {
        const { getByLabelText, getByRole } = render(<AddComponent />);

        expect(getByLabelText('Name')).toBeInTheDocument();
        expect(getByLabelText('Amount')).toBeInTheDocument();
        expect(getByLabelText('Start date')).toBeInTheDocument();
        expect(getByLabelText('Frequency')).toBeInTheDocument();
        expect(getByRole('button')).toBeInTheDocument();
        expect(getByRole('button').textContent).toEqual('Add new payment');
    });

    describe('when the form is submitted', () => {
        it('should redirect to "/" on successful response', async () => {
            const postPaymentSpy = jest.spyOn(PaymentApi, 'postPayment')
                .mockResolvedValue({
                    id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                    name: 'Rent',
                    amount: 100,
                    startDate: '2021-02-19',
                    frequency: PaymentApi.PaymentFrequency.MONTHLY
                });

            const dispatchMock = jest.fn();
            jest.spyOn(ReactRedux, 'useDispatch').mockReturnValue(dispatchMock);

            const { getByLabelText, getByRole, getByText } = render(<AddComponent />);

            await userEvent.type(getByLabelText('Name'), 'Rent');
            await userEvent.type(getByLabelText('Amount'), '100');
            await userEvent.type(getByLabelText('Start date'), '2021-02-19');
            userEvent.selectOptions(getByLabelText('Frequency'), PaymentApi.PaymentFrequency.MONTHLY);
            userEvent.click(getByRole('button'));

            await wait(() => {
                expect(postPaymentSpy).toHaveBeenCalledWith({
                    name: 'Rent',
                    amount: 100,
                    startDate: '2021-02-19',
                    frequency: PaymentApi.PaymentFrequency.MONTHLY
                });
            });

            expect(dispatchMock).toHaveBeenCalledWith({
                payload: {
                    id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                    name: 'Rent',
                    amount: 100,
                    startDate: '2021-02-19',
                    frequency: PaymentApi.PaymentFrequency.MONTHLY
                },
                type: expect.any(String)
            });

            expect(getByText('Home')).toBeInTheDocument();
        });
    });
});