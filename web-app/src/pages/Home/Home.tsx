import React from 'react';
import {Link} from 'react-router-dom';
import {useTypedSelector} from '../../state/helper';
import Layout from '../../Layout/Layout';
import './home.css';

const Home = () => {
    const payments = useTypedSelector((store) => store.payments);

    return (
        <Layout heading="Regular Payments">
            <ul>
                {payments.map((payment) =>
                    <li key={payment.id}>
                        <Link to={`/payment/${payment.id}`} className="paymentItem">
                            <div className="nameAndAmount">
                                <p>{payment.name}</p>
                                <p>&pound;{payment.amount}</p>
                            </div>
                            <div className="dateAndFrequency">
                                <p>{payment.startDate}</p>
                                <p>{payment.frequency}</p>
                            </div>
                        </Link>
                    </li>
                )}
            </ul>
            <div className="addButtonContainer">
                <Link to="/add">Add a bill</Link>
            </div>
        </Layout>
    );
};

export default Home;
