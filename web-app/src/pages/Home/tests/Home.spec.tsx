import React from 'react';
import {configureStore} from '@reduxjs/toolkit';
import * as ReactRedux from 'react-redux';
import {MemoryRouter} from 'react-router-dom';
import {render} from '@testing-library/react';
import Home from '../Home';
import * as PaymentApi from '../../../api/paymentApi';
import paymentsReducer from '../../../state/payments';
import * as StateHelper from '../../../state/helper';

describe('Home', () => {

    const HomeComponent = () => {
        const storeMock = configureStore({reducer: {payments: paymentsReducer}});
        return (
            <ReactRedux.Provider store={storeMock}>
                <MemoryRouter>
                    <Home />
                </MemoryRouter>
            </ReactRedux.Provider>
        );
    };

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should render a list of payments', () => {
        const initialState = {
            payments:  [{
                id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                name: 'Rent',
                amount: 100,
                startDate: '2021-02-19',
                frequency: PaymentApi.PaymentFrequency.MONTHLY
            }, {
                id: '19f98168-ed90-4590-93d5-128701501141',
                name: 'Netflix',
                amount: 9.99,
                startDate: '2021-02-19',
                frequency: PaymentApi.PaymentFrequency.MONTHLY
            }]
        };
        jest.spyOn(StateHelper, 'useTypedSelector').mockImplementation((fn) => fn(initialState));

        const { getByText, getAllByRole } = render(<HomeComponent />);

        expect(getAllByRole('listitem')).toHaveLength(2);
        expect(getByText('Rent')).toBeInTheDocument();
        expect(getByText('Netflix')).toBeInTheDocument();
        expect((getAllByRole('link')[0] as HTMLAnchorElement).href)
            .toEqual('http://localhost/payment/75b1ce35-093e-48e9-9f49-69dc5b670554');
        expect((getAllByRole('link')[1] as HTMLAnchorElement).href)
            .toEqual('http://localhost/payment/19f98168-ed90-4590-93d5-128701501141');
    });
});
