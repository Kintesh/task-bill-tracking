import React from 'react';
import {configureStore} from '@reduxjs/toolkit';
import * as ReactRedux from 'react-redux';
import {MemoryRouter, Route, Switch} from 'react-router-dom';
import {render, wait} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Payment from '../Payment';
import * as PaymentApi from '../../../api/paymentApi';
import paymentsReducer from '../../../state/payments';
import * as StateHelper from '../../../state/helper';

describe('Payment', () => {

    const PaymentComponent = () => {
        const storeMock = configureStore({reducer: {payments: paymentsReducer}});
        return (
            <ReactRedux.Provider store={storeMock}>
                <MemoryRouter initialEntries={['/payment/75b1ce35-093e-48e9-9f49-69dc5b670554']}>
                    <Switch>
                        <Route exact path="/">
                            <p>Home</p>
                        </Route>
                        <Route path="/payment/:id">
                            <Payment />
                        </Route>
                    </Switch>
                </MemoryRouter>
            </ReactRedux.Provider>
        );
    };

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should render edit form if a payment exists', async () => {
        const initialState = {
            payments:  [{
                id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                name: 'Rent',
                amount: 100,
                startDate: '2021-02-19',
                frequency: PaymentApi.PaymentFrequency.MONTHLY
            }]
        };
        jest.spyOn(StateHelper, 'useTypedSelector').mockImplementation((fn) => fn(initialState));

        const { getByLabelText, getByText, getAllByRole } = render(<PaymentComponent />);

        expect(getByLabelText('Name')).toBeInTheDocument();
        expect(getByLabelText('Amount')).toBeInTheDocument();
        expect(getByLabelText('Start date')).toBeInTheDocument();
        expect(getByLabelText('Frequency')).toBeInTheDocument();
        expect(getAllByRole('button')).toHaveLength(2);
        expect(getByText('Save')).toBeInTheDocument();
        expect(getByText('Delete')).toBeInTheDocument();
        expect((getByLabelText('Name') as HTMLInputElement).value).toEqual('Rent');

    });

    it('should redirect to "/" if the does not payment exist', async () => {
        const { getByText } = render(<PaymentComponent />);

        expect(getByText('Home')).toBeInTheDocument();
    });

    describe('when the form is submitted (payment is saved/updated)', () => {
        it('should redirect to "/" on successful response', async () => {
            const patchPaymentSpy = jest.spyOn(PaymentApi, 'patchPayment')
                .mockResolvedValue({
                    id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                    name: 'Rent',
                    amount: 1000,
                    startDate: '2021-02-19',
                    frequency: PaymentApi.PaymentFrequency.ANNUALLY
                });

            const dispatchMock = jest.fn();
            jest.spyOn(ReactRedux, 'useDispatch').mockReturnValue(dispatchMock);

            const initialState = {
                payments:  [{
                    id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                    name: 'Rent',
                    amount: 100,
                    startDate: '2021-02-19',
                    frequency: PaymentApi.PaymentFrequency.MONTHLY
                }]
            };
            jest.spyOn(StateHelper, 'useTypedSelector').mockImplementation((fn) => fn(initialState));

            const { getByLabelText, getByText } = render(<PaymentComponent />);

            await userEvent.type(getByLabelText('Name'), 'Rent');
            await userEvent.type(getByLabelText('Amount'), '1000');
            await userEvent.type(getByLabelText('Start date'), '2021-02-19');
            userEvent.selectOptions(getByLabelText('Frequency'), PaymentApi.PaymentFrequency.ANNUALLY);
            userEvent.click(getByText('Save'));

            await wait(() => {
                expect(patchPaymentSpy).toHaveBeenCalledWith('75b1ce35-093e-48e9-9f49-69dc5b670554', {
                    name: 'Rent',
                    amount: 1000,
                    startDate: '2021-02-19',
                    frequency: PaymentApi.PaymentFrequency.ANNUALLY
                });
            });

            expect(dispatchMock).toHaveBeenCalledWith({
                payload: {
                    id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                    name: 'Rent',
                    amount: 1000,
                    startDate: '2021-02-19',
                    frequency: PaymentApi.PaymentFrequency.ANNUALLY
                },
                type: expect.any(String)
            });

            expect(getByText('Home')).toBeInTheDocument();
        });
    });
    describe('when the payment is deleted', () => {
        it('should redirect to "/" on successful response', async () => {
            const removePaymentSpy = jest.spyOn(PaymentApi, 'removePayment').mockResolvedValue(true);

            const dispatchMock = jest.fn();
            jest.spyOn(ReactRedux, 'useDispatch').mockReturnValue(dispatchMock);

            const initialState = {
                payments:  [{
                    id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                    name: 'Rent',
                    amount: 100,
                    startDate: '2021-02-19',
                    frequency: PaymentApi.PaymentFrequency.MONTHLY
                }]
            };
            jest.spyOn(StateHelper, 'useTypedSelector').mockImplementation((fn) => fn(initialState));

            const { getByLabelText, getByText } = render(<PaymentComponent />);

            await userEvent.type(getByLabelText('Name'), 'Rent');
            await userEvent.type(getByLabelText('Amount'), '1000');
            await userEvent.type(getByLabelText('Start date'), '2021-02-19');
            userEvent.selectOptions(getByLabelText('Frequency'), PaymentApi.PaymentFrequency.ANNUALLY);
            userEvent.click(getByText('Delete'));

            await wait(() => {
                expect(removePaymentSpy).toHaveBeenCalledWith('75b1ce35-093e-48e9-9f49-69dc5b670554');
            });

            expect(dispatchMock).toHaveBeenCalledWith({
                payload: {
                    id: '75b1ce35-093e-48e9-9f49-69dc5b670554',
                    name: 'Rent',
                    amount: 100,
                    startDate: '2021-02-19',
                    frequency: PaymentApi.PaymentFrequency.MONTHLY
                },
                type: expect.any(String)
            });

            expect(getByText('Home')).toBeInTheDocument();
        });
    });
});