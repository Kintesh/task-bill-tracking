import React from 'react';
import {Redirect, useHistory, useParams} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import {Formik, FormikProps} from 'formik';
import Layout from '../../Layout/Layout';
import {patchPayment, Payment as PaymentType, PaymentFrequency, removePayment} from '../../api/paymentApi';
import {updatePayment, deletePayment} from '../../state/payments';
import {useTypedSelector} from '../../state/helper';
import PaymentForm, {PaymentFormValues} from '../../components/PaymentForm';
import './payment.css';

const Payment = () => {
    const dispatch = useDispatch();
    const { id } = useParams<{id: string}>();
    const history = useHistory();

    const payment = useTypedSelector((store) => store.payments.find(p => p.id === id));

    if (payment === undefined) {
        return <Redirect to="/" />;
    }
    
    const initialValues: PaymentFormValues = {
        name: payment.name,
        amount: payment.amount,
        startDate: payment.startDate,
        frequency: PaymentFrequency.WEEKLY,
    };

    const handleDeleteButtonClick = () => {
        removePayment(payment.id).then(() => {
            dispatch(deletePayment(payment));
            history.push('/');
        });
    };

    return (
        <Layout heading="Edit a bill">
            <h1 className="center">{payment.name}</h1>
            <p className="center">If you&apos;d like to edit your bill you can change the<br/>details below</p>
            <Formik initialValues={initialValues}
                onSubmit={(values, formikHelpers) => {
                    console.log('Payment onSubmit', values);
                    patchPayment(payment.id, values as PaymentType).then((payment) => {
                        console.log('patchPayment - then', payment);
                        dispatch(updatePayment(payment));
                        formikHelpers.setSubmitting(false);
                        history.push('/');
                    });
                }}
            >
                {(props: FormikProps<PaymentFormValues>) => (
                    <PaymentForm {...props}>
                        <div className="saveAndDeleteButtonContainer">
                            <button className="save" type="submit">Save</button>
                            <br/><br/>
                            <button className="delete" type="button" onClick={handleDeleteButtonClick}>Delete</button>
                        </div>
                    </PaymentForm>
                )}
            </Formik>
        </Layout>
    );
};

export default Payment;
