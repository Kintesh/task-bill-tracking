import React from 'react';
import './layout.css';

interface LayoutProps {
    heading: string;
    children: React.ReactNode;
}

const Layout = (props: LayoutProps) => {
    return (
        <div>
            <header>{props.heading}</header>
            <main>{props.children}</main>
        </div>
    );
};

export default Layout;
