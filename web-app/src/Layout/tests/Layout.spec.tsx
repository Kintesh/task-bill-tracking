import React from 'react';
import {render} from '@testing-library/react';
import Layout from '../Layout';

describe('Layout', () => {
    it('should render layout with header and main', () => {
        const { getByText, getByRole } = render(
            <Layout
                heading="Some heading"
            >
                <p>Some main content</p>
            </Layout>
        );

        expect(getByRole('main')).toBeInTheDocument();
        expect(getByText('Some heading')).toBeInTheDocument();
        expect(getByText('Some main content')).toBeInTheDocument();
    });
});