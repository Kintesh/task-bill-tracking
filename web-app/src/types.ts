import {ChangeEventHandler, FocusEventHandler} from 'react';

export interface FormElementProps<T> {
    label: string;
    name: string;
    value: string | number;
    onChange: ChangeEventHandler<T>
    onBlur: FocusEventHandler<T>

}
